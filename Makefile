CCFLAGS = -std=c++17 -g -O3 -I /usr/local/include -lOpenGL
LDFLAGS = -L/usr/local/lib -lglfw -lBox2D

OBJ = main.o world.o robot.o box.o guiworld.o
HDR = push.hh pusher.hh

all: push

push: $(OBJ) $(HDR)
	g++ $(CCFLAGS) $(OBJ) $(LDFLAGS) -o $@

%.o: %.cc $(HDR)
	g++ $(CCFLAGS) -c $<

clean:
	rm -f push 
	rm -f *.o
