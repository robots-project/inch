#include "push.hh"
#include <iostream>


// constructor
Robot::Robot( World& world, 
	      float x, 
	      float y, 
	      float a, 
	      float size,
	      float charge,
	      float charge_max,
	      float input_efficiency,
	      float output_metabolic, 
	      float output_efficiency ) : 
  world(world),
  size( size ),
  charge(charge),
  charge_max(charge_max),
  charge_delta(0),
  input_efficiency(input_efficiency),
  output_metabolic(output_metabolic),
  output_efficiency(output_efficiency),
  body( NULL ),
  front_bumper( NULL ),
  back_bumper( NULL ),
  front_joint( NULL ),
  back_joint( NULL )
{
  b2BodyDef bodyDef;
  bodyDef.type = b2_dynamicBody;
  body = world.b2world->CreateBody(&bodyDef);
  front_bumper = world.b2world->CreateBody(&bodyDef);
  back_bumper = world.b2world->CreateBody(&bodyDef);
  
  b2PolygonShape dynamicBox;
  dynamicBox.SetAsBox( size/2.0, size/2.0 );
  
  b2FixtureDef fixtureDef;
  fixtureDef.shape = &dynamicBox;    
  fixtureDef.density = 10;
  fixtureDef.friction = 1.0;

  // prevent collision with puck-retaining strings 
  fixtureDef.filter.categoryBits = ROBOT;
  fixtureDef.filter.maskBits = ROBOT | BOX | ROBOTBOUNDARY; // not box boundary

  body->CreateFixture(&fixtureDef);
  
  // bumper has same settings the body but different size
  dynamicBox.SetAsBox( size/10.0, size/2.0 );
  front_bumper->CreateFixture(&fixtureDef);
  back_bumper->CreateFixture(&fixtureDef);

  
  b2PrismaticJointDef front_jointDef;
  b2PrismaticJointDef back_jointDef;
  
  front_jointDef.Initialize( body, 
		       front_bumper, 
		       body->GetWorldCenter(), 
		       b2Vec2( 1.0f, 0.0f )
		       ); 
  back_jointDef.Initialize( back_bumper,  
		       body,
		       body->GetWorldCenter(), 
		       b2Vec2( 1.0f, 0.0f )
		       ); 
  
  front_jointDef.lowerTranslation = 0;//-0.2;
  front_jointDef.upperTranslation = 0.04f;
  front_jointDef.enableLimit = true;
  front_jointDef.maxMotorForce = 0.8f;
  front_jointDef.motorSpeed = 1.0f;
  front_jointDef.localAnchorA.Set( size/2.0, 0); // on the nose
  front_jointDef.localAnchorB.Set( 0,0 );

  front_jointDef.enableMotor = true;
  //jointDef.collideConnected = true;
  
  front_joint = (b2PrismaticJoint*)world.b2world->CreateJoint( &front_jointDef );

  back_jointDef.lowerTranslation = 0;//-0.2;
  back_jointDef.upperTranslation = 0.04f;
  back_jointDef.enableLimit = true;
  back_jointDef.maxMotorForce = 0.8f;
  back_jointDef.motorSpeed = 1.0f;
  back_jointDef.localAnchorA.Set( 0, 0); // on the nose
  back_jointDef.localAnchorB.Set( -size/2.0,0 );

  back_jointDef.enableMotor = true;
  //jointDef.collideConnected = true;
  
  back_joint = (b2PrismaticJoint*)world.b2world->CreateJoint( &back_jointDef );

  // place assembled robot in the world
  body->SetTransform( b2Vec2( x, y ), a );	
  front_bumper->SetTransform( body->GetWorldPoint( b2Vec2( size/2, 0) ), a );	
  back_bumper->SetTransform( body->GetWorldPoint( b2Vec2( -size/2, 0) ), a );	
}

float Robot::GetLightIntensity( void )
{
  const b2Vec2 here = body->GetWorldCenter();  
  return world.GetLightIntensityAt( here.x, here.y );
}

bool Robot::GetBackBumperPressed( void )
{
  return( back_joint->GetJointTranslation() < 0.0 && front_joint->GetJointTranslation() > 0.0 );
}

void Robot::PrintBumper( bool direction )
{
  //std::cout << "BACK TRANSLATION: " << back_joint->GetJointTranslation() << std::endl;
  //std::cout << "FRONT TRANSLATION: " << front_joint->GetJointTranslation() << std::endl;

  b2Vec2 backForce = back_joint->GetReactionForce(1);
  b2Vec2 frontForce = front_joint->GetReactionForce(1);

  double backForceMag = sqrt(pow(backForce.x, 2) + pow(backForce.y, 2));
  double frontForceMag = sqrt(pow(frontForce.x, 2) + pow(frontForce.y, 2));

  std::cout << std::endl;
  if (direction) {
    std::cout << "FRONT FORCE MAG: " << frontForceMag << std::endl;
  } else {
    std::cout << "BACK FORCE MAG: " << backForceMag << std::endl;
  }
}

bool Robot::GetFrontBumperPressed( void )
{
  return( front_joint->GetJointTranslation() < 0.0 && back_joint->GetJointTranslation() > 0.0 );
}

double Robot::GetFrontBumperForce( void ) const
{
  b2Vec2 frontForce = front_joint->GetReactionForce(1);
  double frontForceMag = sqrt(pow(frontForce.x, 2) + pow(frontForce.y, 2));
  return frontForceMag;
}

double Robot::GetBackBumperForce( void ) const
{
  b2Vec2 backForce = back_joint->GetReactionForce(1);
  double backForceMag = sqrt(pow(backForce.x, 2) + pow(backForce.y, 2));
  return backForceMag;
}

// set body speed in body-local coordinate frame
void Robot::SetSpeed( float x, float y, float a )
{  
  body->SetLinearVelocity( body->GetWorldVector(b2Vec2( x, y )));
  body->SetAngularVelocity( a );
}

void Robot::Update( float timestep )
{
  // absorb energy from lights
  charge_delta = input_efficiency * GetLightIntensity(); // gather power from light

  // expend energy just living  
  charge_delta -= output_metabolic; 
  
  // expend energy by moving
  charge_delta -= output_efficiency * body->GetAngularVelocity(); 
  charge_delta -= output_efficiency * body->GetLinearVelocity().Length();

  charge += charge_delta;
  
  if( charge <= 0.0 )
    {
      charge = 0.0;
      SetSpeed( 0,0,0 ); // can't move
    }
  else if( charge > charge_max ) 
    charge = charge_max; // full up
}
