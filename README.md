# Box packing robots

This is a fork of [push](https://github.com/rtv/push) with an improved robot design and controller.

## How To Run

The simulator requires OpenGL and Box2D to compile. To build, just run `make`.

To reproduce experiments, simply run

    $ ./push --pose-files n

where `n` corresponds to `n.txt` in the `positions/box/` and `positions/robot/` directories.

To produce pose files (to be used as above), run

    $ ./push --gen-poses

then you can move the provided position files to files under `positions/box/` and `positions/robot/`.
