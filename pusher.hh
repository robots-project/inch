#include <cmath>
#include <string>
#include <iostream>
#include <future>

#include "push.hh"

int ID = 0;

#define HISTLEN 100
#define BUMPHIST 20

std::string to_string(bool p)
{
  return p ? "1" : "0";
}

bool header = true;
void Log(int id,
    int time,
    bool bumped_ahead,
    bool bumped_behind,
    double avg_behind,
    double avg_ahead,
    bool still_hitting,
    int time_since_bump,
    double bump_force,
    double avg_box_dist
    )
{
  if (header) {
    std::cout << "id,time,bumped_ahead,bumped_behind,avg_behind,avg_ahead,still_hitting,time_since_bump,bump_force,avg_box_dist" << std::endl;
    header = false;
  }
  std::string str_id = std::to_string(id);
  std::string str_time = std::to_string(time);
  std::string str_bumped_ahead = to_string(bumped_ahead);
  std::string str_bumped_behind = to_string(bumped_behind);
  std::string str_avg = std::to_string(avg_behind);

  std::cout << str_id
    << "," << str_time
    << "," << str_bumped_ahead
    << "," << str_bumped_behind
    << "," << str_avg
    << "," << avg_ahead
    << "," << still_hitting
    << "," << time_since_bump
    << "," << bump_force
    << "," << avg_box_dist
    << '\n';
}

class Pusher : public Robot
{
public:
  virtual void Update( float timestep )
  {
    if (time++ == *steps) exit(0);

    bool bumped_ahead = false;
    bool bumped_behind = false;
    bool still_hitting = false;

    time_since_bump++;

    // if nothing is happening, full speed ahead
    speeda = 0;

    // back up with probability tenacity
    // keep going with probability 1 - tenacity
    //double tenacity = 0.33;
    // keep going for tenacity_duration steps when you bump and decide not to back up
    unsigned tenacity_duration = 10;

    // on the first impact on your bumper
    if (BumpedAhead() && ignore_bumps == 0) {
      if (rand() < (tenacity * RAND_MAX)) {
        speedx = -speedx;
        bumped_ahead = true;
        // each bump registers as 5 bumps in the following 10 steps, for some reason...
        ignore_bumps = tenacity_duration;
        time_since_bump = 0;
        positive_direction = !positive_direction;
      }
    } else if (BumpedAhead() && ignore_bumps > 0) {
      // we're hitting boxes probably...
      still_hitting = true;
    }

    // if you get bumped, turn a bit
    if (BumpedBehind()) {
      speeda = .1;
      //speeda = drand48() * (M_PI/2);
      bumped_behind = true;
    }

    double bump_force = 0.0;
    if (bumped_ahead) {
      bump_force = BumpedAheadForce();
      prev_bump_forces[num_prev_bumps++] = bump_force;
    }

    if (ignore_bumps > 0) ignore_bumps--;


    SetSpeed( speedx, speedy, speeda );

    // maintain average of bumps in past 100 steps
    int oldest_behind = prev_bumps_behind[time % history_len] ? 1 : 0;
    if (bumped_behind) {
      sum_bumps_behind += (1 - oldest_behind);
    } else {
      sum_bumps_behind -= oldest_behind;
    }
    prev_bumps_behind[time % history_len] = bumped_behind;
    double avg_behind = (double)sum_bumps_behind / history_len;

    int oldest = prev_bumps_ahead[time % history_len] ? 1 : 0;
    if (bumped_ahead) {
      sum_bumps_ahead += (1 - oldest);
    } else {
      sum_bumps_ahead -= oldest;
    }
    prev_bumps_ahead[time % history_len] = bumped_ahead;
    double avg_ahead = (double)sum_bumps_ahead / history_len;

    double avg_bump_force = 0;
    if (num_prev_bumps == BUMPHIST) {
      for (int i = 0; i < BUMPHIST; i++) {
        avg_bump_force += prev_bump_forces[i];
      }
      avg_bump_force = avg_bump_force / BUMPHIST;
      num_prev_bumps = 0;
    }

    //converged = avg_bump_force > 0.24;
    //if (converged) speedx = speeda = 0;


    //(*steps)--;
#ifdef LOG
    Log(id, time, bumped_ahead, bumped_behind, avg_behind, avg_ahead, still_hitting, time_since_bump, bump_force, world.avg_box_dist);
#endif

    Robot::Update( timestep ); // inherit underlying behaviour to handle charge/discharge
  }

  Pusher( World& world, float size, float x, float y, float a, size_t *steps, double tenacity ) : 
    Robot( world, 
	   x,y,a,
	   size,
	   100,
	   100,
	   1,0,0 ), // stay charged forever 
    timeleft( drand48() * TURNMAX ),
    speedx( 1 ),
    speedy( 0 ),
    speeda( 0 ),
    time( 0 ),
    steps( steps ),
    id( ID++ ),
    tenacity( tenacity )
  {}

private:
  bool BumpedAhead()
  {
    return (GetFrontBumperPressed() && positive_direction) || (GetBackBumperPressed() && !positive_direction);
  }

  bool BumpedBehind()
  {
    return (GetFrontBumperPressed() && !positive_direction) || (GetBackBumperPressed() && positive_direction);
  }

  double BumpedAheadForce()
  {
    return positive_direction ? GetFrontBumperForce() : GetBackBumperForce();
  }
  
  static const size_t history_len;
  static const float PUSH, BACKUP, TURNMAX;
  static const float SPEEDX, SPEEDA;
  static const float maxspeedx, maxspeeda;
  
  float timeleft;
  float speedx, speedy, speeda;

  bool positive_direction = true;
  // when the models are first getting settled there are 10 false bumps
  int ignore_bumps = 10;

  // convergence values
  bool converged = false;
  int sum_bumps_behind = 0;
  bool prev_bumps_behind[HISTLEN] = {false};
  int sum_bumps_ahead = 0;
  bool prev_bumps_ahead[HISTLEN] = {false};

  double prev_bump_forces[BUMPHIST] = {0};
  size_t num_prev_bumps = 0;

  int time_since_bump = 0;

  int time;
  size_t *steps;
  int id;

  double tenacity;
}; // class Pusher

// static members
const size_t Pusher::history_len = HISTLEN;
const float Pusher::PUSH = 15.0; // seconds
const float Pusher::BACKUP = 0.5;
const float Pusher::TURNMAX = 2.0;
const float Pusher::SPEEDX = 0.5;
const float Pusher::SPEEDA = M_PI/2.0;
const float Pusher::maxspeedx = 0.5;
const float Pusher::maxspeeda = M_PI/2.0;
