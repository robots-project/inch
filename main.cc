
#include <stdio.h>
#include <getopt.h>
#include <unistd.h> // for usleep(3)

#include <vector>
#include <set>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include <ctime>

#include "push.hh"
#include "pusher.hh"


void makeRandomPoses(double width, double height, size_t robots, size_t boxes) {
  // seed
  srand48(time(NULL));
  // and burn first value
  drand48();

  std::ofstream robot_file;
  robot_file.open("robot_positions.txt");
  for (size_t i = 0; i < robots; i++ ) {
    float x = drand48() * width;
    float y = drand48() * height;
    float a = drand48() * M_PI;

    while( x > width*0.2 && x < width*0.65 && y > height*0.2 && y < height*0.65 ) {
      x = drand48() * width;
      y = drand48() * height;
    }

    robot_file << x << " " << y << " " << a << "\n";
  }
  robot_file.close();

  std::ofstream box_file;
  box_file.open("box_positions.txt");
  for (size_t i = 0; i < boxes; i++) {
    float x = width/6.0 + drand48()*width*0.7;
    float y = height/6.0 + drand48()*height*0.7;
    float a = drand48() * M_PI;

    box_file << x << " " << y << " " << a << "\n";
  }
  box_file.close();
}

void drawEverything(World &world, float robot_size, float box_size, size_t *steps, size_t pose_files, double tenacity, bool hex) {

  std::ifstream box_file(pose_files ? "positions/box/" + std::to_string(pose_files) + ".txt" : "box_positions.txt");
  std::ifstream robot_file(pose_files ? "positions/robot/" + std::to_string(pose_files) + ".txt" : "robot_positions.txt");

  std::string line;
  if (box_file.is_open()) {
    while (getline(box_file, line)) {
      size_t pos = line.find(" ");
      std::string rest = line.substr(pos + 1, std::string::npos);
      size_t new_pos = rest.find(" ");

      std::istringstream x_stream(line.substr(0, pos));
      std::istringstream y_stream(line.substr(pos, new_pos));
      std::istringstream a_stream(line.substr(new_pos, std::string::npos));

      double x, y, a;
      x_stream >> x;
      y_stream >> y;
      a_stream >> a;

      world.AddBox(new Box( world, hex ? Box::SHAPE_HEX : Box::SHAPE_RECT, box_size, x, y, a));
    }

    box_file.close();
  }


  if (robot_file.is_open()) {
    while (getline(robot_file, line)) {
      size_t pos = line.find(" ");
      std::string rest = line.substr(pos + 1, std::string::npos);
      size_t new_pos = rest.find(" ");

      std::istringstream x_stream(line.substr(0, pos));
      std::istringstream y_stream(line.substr(pos, new_pos));
      std::istringstream a_stream(line.substr(new_pos, std::string::npos));

      double x, y, a;
      x_stream >> x;
      y_stream >> y;
      a_stream >> a;

      world.AddRobot( new Pusher( world, robot_size, x, y, a, steps, tenacity ));
    }
    robot_file.close();
  }
}

void runGui(float WIDTH, float HEIGHT, size_t ROBOTS, size_t BOXES, float timeStep, float robot_size, float box_size, size_t steps, size_t arena, double tenacity, size_t pose_files, bool hex)
{
  GuiWorld world(WIDTH, HEIGHT, arena);
  
  drawEverything(world, robot_size, box_size, &steps, pose_files, tenacity, hex);

  /* Loop until the user closes the window */
  while( !world.RequestShutdown())
  {
    world.Step( timeStep );	  
  }
}

void runHeadless(float WIDTH, float HEIGHT, size_t ROBOTS, size_t BOXES, float timeStep, float robot_size, float box_size, size_t steps, size_t arena, double tenacity, size_t pose_files, bool hex)
{
  World world(WIDTH, HEIGHT, arena);

  drawEverything(world, robot_size, box_size, &steps, pose_files, tenacity, hex);

  while( true )
  {
    world.Step( timeStep );	  
  }
}


#define USAGE "Options:\n\
  -?,\t--help\tDisplay this message\n\
  -w,\t--width\tArena width (default: 16)\n\
  -h,\t--height\tArena height (default: 16)\n\
  -r,\t--robots\tNumber of robots (default: 48)\n\
  -b,\t--boxes\tNumber of boxes (default: 512)\n\
  -s,\t--steps\tNumber of simulation steps to run, after which the program terminates\n\
  -t,\t--tenacity\tTenacity measure for robots (they will reverse direction with probability tenacity) (default: 0.33)\n\
  -q,\t--headless\tHeadless mode for superior performance when running taxing simulations\n\
  -a,\t--arena\tNumber of walls of arena (default: 4)\n\
  -g,\t--gen-poses\tGenerate a file containing random poses for boxes and robots\n\
  -p,\t--pose-files\tIndicate which pose-file set to use from the ones in positions (for example: -p 1 uses 1.txt in positions/box/ and positions/robot/)\n\
  -y,\t--box-shape\tShape of boxes (\"hex\" or \"square\", default: hex)\n\
  -u,\t--box-size\tSize of boxes (default: 0.3)\n"

int main( int argc, char* argv[] )
{
  float WIDTH = 16;
  float HEIGHT = 16;
  size_t ROBOTS = 48;//48;
  size_t BOXES = 512;
  float timeStep = 1.0 / 30.0;
  float robot_size = 0.3;
  float box_size = 0.3;
  size_t steps = -1;
  std::string box_shape = "hex";
  bool headless = false;
  bool gen_poses = false;
  size_t arena = 4;
  double tenacity = .33;
  size_t pose_files = 0;

  /* options descriptor */
  static struct option longopts[] = {
    { "width",  required_argument,   NULL,  'w' },
    { "height",  required_argument,   NULL,  'h' },
    { "robots",  required_argument,   NULL,  'r' },
    { "boxes",  required_argument,   NULL,  'b' },
    //{ "robotsize",  required_argument,   NULL,  'z' },
    //{ "boxsize",  required_argument,   NULL,  's' },
    { "steps", required_argument, NULL, 's' },
    { "arena", required_argument, NULL, 'a' },
    { "box-shape", required_argument, NULL, 'y'},
    { "box-size", required_argument, NULL, 'u'},
    { "tenacity", required_argument, NULL, 't' },
    { "pose-files", required_argument, NULL, 'p' },
    { "headless", 0, NULL, 'q' },
    { "gen-poses", 0, NULL, 'g' },
    { "help",  0,   NULL,  '?' },
    { NULL, 0, NULL, 0 }
  };
  
  int ch=0, optindex=0;  
  while ((ch = getopt_long(argc, argv, "w:h:r:b:s:z:q:t:g:p:", longopts, &optindex)) != -1)
  {
    switch( ch )
    {
      case 0: // long option given
        printf( "option %s given", longopts[optindex].name );
        if (optarg)
          printf (" with arg %s", optarg);
        printf ("\n");
        break;
      case 'w':
        WIDTH = atof( optarg );
        break;
      case 'h':
        HEIGHT = atof( optarg );
        break;
      case 'r':
        ROBOTS = atoi( optarg );
        break;
      case 'b':
        BOXES = atoi( optarg );
        break;
      case 's':
        steps = atoi( optarg );
        break;
      case 't':
        tenacity = atof( optarg );
        break;
      case 'q':
        headless = true;
        break;
      case 'a':
        arena = atoi( optarg );
        break;
      case 'g':
        gen_poses = true;
        break;
      case 'p':
        pose_files = atoi(optarg);
        break;
      case 'y':
        box_shape = optarg;
        break;
      case 'u':
        box_size = atof(optarg);
        break;
      default:
        printf("Unrecognized option %c\n", ch );
        printf( "%s [OPTIONS]\n", argv[0] );
        puts( USAGE );
        exit(0);
    }
  }

  if (!pose_files) {
    makeRandomPoses(WIDTH, HEIGHT, ROBOTS, BOXES);
  }

  if (gen_poses) {
    makeRandomPoses(WIDTH, HEIGHT, ROBOTS, BOXES);
  } else if (headless) {
    runHeadless(WIDTH, HEIGHT, ROBOTS, BOXES, timeStep, robot_size, box_size, steps, arena, tenacity, pose_files, box_shape == "hex");
  } else {
    runGui(WIDTH, HEIGHT, ROBOTS, BOXES, timeStep, robot_size, box_size, steps, arena, tenacity, pose_files, box_shape == "hex");
  }

  return 0;
}
