#include "push.hh"
#include <iostream>
#include <unordered_map>
#include <limits>
#include <algorithm>
#include <numeric>

World::World( float width, float height, size_t arena) :
  b2world( new b2World( b2Vec2( 0,0 ))), // gravity 
  width(width),
  height(height),
  walln(arena),
  steps(0),
  lights() //empty vector
{
    // set exterior box container
    b2BodyDef boxWallDef1;
    b2PolygonShape groundBox1;

    // set vertices of n-gon
    double radius = width * .8;
    double angle = 0;
    double angle_increment = (2*M_PI) / walln;
    b2Vec2 center(width / 2, height / 2);
    b2Vec2 points[walln];

    for ( size_t i = 0; i < walln; i++ ) {
      points[i].x = center.x + radius * cos(angle + M_PI/4);
      points[i].y = center.y + radius * sin(angle + M_PI/4);
      angle += angle_increment;
    }

    // regular: so we can use any 2 points
    double side_length = (points[1] - points[0]).Length() / 2;
    groundBox1.SetAsBox( side_length, 0.01f );    
    
    b2FixtureDef fixtureDef1;
    fixtureDef1.shape = &groundBox1;
    
    // prevent collision with puck-retaining strings 
    fixtureDef1.filter.categoryBits = ROBOTBOUNDARY;
    fixtureDef1.filter.maskBits = ROBOT | BOX; // contain everthing
  
    
    for ( size_t i = 0; i < walln; i++ ) {
      // draw a line between vertex i and vertex i+1
      b2Vec2 vi = points[i];
      b2Vec2 vip1 = points[(i+1) % walln];

      double slope = (vi.y - vip1.y) / (vi.x - vip1.x);
      double slope_angle = atan(slope);
      b2Vec2 center_point((vi.x + vip1.x) / 2, (vi.y + vip1.y) / 2);

      robotWall.push_back(b2world->CreateBody(&boxWallDef1));
      //robotWall[i] = b2world->CreateBody(&boxWallDef1);
      robotWall[i]->CreateFixture(&fixtureDef1);

      robotWall[i]->SetTransform(center_point, slope_angle);
    }
}

void World::AddLight( Light* l )
{
  lights.push_back( l );
}

void World::AddLightGrid( size_t xcount, size_t ycount, float z, float intensity )
{
  float xspace = width/(float)xcount;
  float yspace = height/(float)ycount;
  
  for( size_t y=0; y<ycount; y++ )
    for( size_t x=0; x<xcount; x++ )
      AddLight( new Light(  x * xspace + xspace/2.0, 
			    y * yspace + yspace/2.0,
			    z,
			    intensity ) );
}

void World::AddRobot( Robot* r  )
{
  robots.push_back( r );
}

void World::AddBox( Box* b  )
{
  boxes.push_back( b );
}

void World::SetLightIntensity( size_t index, float intensity )
{
  if( index < lights.size() )
    lights[index]->intensity = intensity;
}

float World::GetLightIntensityAt( float x, float y )
{
  // integrate brightness over all light sources
  float total_brightness = 0.0;

  for( auto& l : lights )
    {
      // horizontal and vertical distances
      float dx = x - l->x;
      float dy = y - l->y;
      float dz = l->z;

      float distsquared = dx*dx + dy*dy + dz*dz;
      float dist = sqrt( distsquared );

      // brightness as a function of distance
      float brightness = l->intensity / distsquared;

      // now factor in the angle to the light      
      float theta = atan2( dz, hypot(dx*dx,dy*dy) );

      // and integrate
      total_brightness += brightness * sin(theta);
    }

  return total_brightness;
}

static std::unordered_map<Box *, double> dists;
static std::unordered_map<Box *, b2Vec2> pos;
void World::Step( float timestep )
{
  for( auto& r : robots )
    r->Update( timestep );

  const int32 velocityIterations = 6;
  const int32 positionIterations = 2;
    
  // Instruct the world to perform a single step of simulation.
  // It is generally best to keep the time step and iterations fixed.
  b2world->Step( timestep, velocityIterations, positionIterations);	

  steps++;
  //std::cerr << '\r' << "TIME: " << steps << std::flush;

#ifdef LOG
  // first, for each box get minimum distance to another box
  for (auto &b1 : boxes) {
    b2Vec2 pose = b1->body->GetPosition();
    // skip if the box hasn't moved
    if (pose == pos[b1]) {
      continue;
    }
    pos[b1] = pose;
    double curr_min;
    try {
      curr_min = dists.at(b1);
    } catch (std::exception) {
      curr_min = std::numeric_limits<double>::infinity();
    }

    for (auto &b2 : boxes) {
      if (b1 != b2) {
        double dist = (b1->body->GetPosition() - b2->body->GetPosition()).Length() / 2;
        dists[b1] = std::min(curr_min, dist);
      }
    }
  }

  double sum_dist = std::accumulate(dists.begin(), dists.end(), 0, [](double acc, auto x) -> double {
      return acc + x.second;
  });

  avg_box_dist = sum_dist / boxes.size();
#else
  avg_box_dist = 1;
#endif
}
