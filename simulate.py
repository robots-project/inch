import re
import os

from pathlib import Path

import pandas as pd
import matplotlib.pyplot as plt

def plot_bumps(data, nrobots, nboxes, nsteps, width, height, out):
    timed = data.groupby('time').sum()
    del timed['id']

    plt.figure(figsize=(20, 5))
    plt.title('{} robots and {} boxes in a {}x{} arena'.format(nrobots, nboxes, width, height))
    plt.plot(timed['bumped_ahead'], label='bumped a robot')
    plt.plot(timed['bumped_behind'], label='got bumped')
    plt.xlabel('steps')
    plt.ylabel('occurrences')
    plt.legend()
    
    if not os.path.exists(out):
        os.makedirs(out)
        
    plt.savefig('{}/{}-{}-{}-{}x{}.png'.format(out, nrobots, nboxes, nsteps, width, height))

def analyse(path, outfolder='.'):
    regex = re.compile('data-(\d+)-(\d+)-(\d+)-(\d+)x(\d+).csv')
    m = regex.match(path.name)
    if m is not None:
        nrobots, nboxes, nsteps, width, height = m.groups()
        data = pd.read_csv(str(path))
        plot_bumps(data, nrobots, nboxes, nsteps, width, height, outfolder)

def main():
    time = 600000
    settings = [
        # robots, boxes, width, height
        (24, 256, 8, 8),
        (48, 512, 16, 16), # default settings
        (96, 1024, 32, 32)
    ]

    for setting in settings:
        nrobots, nboxes, width, height = setting
        if not os.path.exists("data/"):
            os.makedirs("data/")
    
        os.system('./push --headless -r {0} -b {1} -t {2} -w {3} -h {4} > data/data-{0}-{1}-{2}-{3}x{4}.csv'\
                  .format(nrobots, nboxes, time, width, height))
    
    pathlist = Path(".").glob('**/data-*.csv')
    for path in pathlist:
        analyse(path, 'out')


if __name__ == '__main__':
    main()
